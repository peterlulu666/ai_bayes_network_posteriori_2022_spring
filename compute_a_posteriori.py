import sys

# The program takes a single command line argument,
# which is a string, for example CLLCCCLLL
try:
    observations = sys.argv[1]
    observations = str(observations).upper()
except IndexError:
    print("we have made no observations yet")
    sys.exit(1)

# h1 (prior: 10%)
h1_prior = 10 / 100
# This type of bag contains 100% cherry candies
h1_cherry = 100 / 100
h1_lime = 0 / 100

# h2 (prior: 20%)
h2_prior = 20 / 100
# This type of bag contains 75% cherry candies and 25% lime candies
h2_cherry = 75 / 100
h2_lime = 25 / 100

# h3 (prior: 40%)
h3_prior = 40 / 100
# This type of bag contains 50% cherry candies and 50% lime candies
h3_cherry = 50 / 100
h3_lime = 50 / 100

# h4 (prior: 20%)
h4_prior = 20 / 100
#  This type of bag contains 25% cherry candies and 75% lime candies
h4_cherry = 25 / 100
h4_lime = 75 / 100

# h5 (prior: 10%)
h5_prior = 10 / 100
# This type of bag contains 100% lime candies
h5_cherry = 0 / 100
h5_lime = 100 / 100

file_handle = open('result.txt', 'w')
file_handle.write("Observation sequence Q: " + observations)
file_handle.write("\nLength of Q: " + str(len(observations)))
file_handle.write("\n")

after_observation = 1
observations_index = 0
while (True):
    file_handle.write("\nAfter Observation " + str(after_observation) + " = " + observations[observations_index])
    file_handle.write("\n")

    probabilities_sum = 0
    if observations[observations_index] == "C":
        probability_1 = h1_prior * h1_cherry
        probability_2 = h2_prior * h2_cherry
        probability_3 = h3_prior * h3_cherry
        probability_4 = h4_prior * h4_cherry
        probability_5 = h5_prior * h5_cherry
    elif observations[observations_index] == "L":
        probability_1 = h1_prior * h1_lime
        probability_2 = h2_prior * h2_lime
        probability_3 = h3_prior * h3_lime
        probability_4 = h4_prior * h4_lime
        probability_5 = h5_prior * h5_lime

    probabilities_sum = probability_1 + probability_2 + \
                        probability_3 + probability_4 + \
                        probability_5

    p_h1_given_Q = probability_1 / probabilities_sum
    p_h2_given_Q = probability_2 / probabilities_sum
    p_h3_given_Q = probability_3 / probabilities_sum
    p_h4_given_Q = probability_4 / probabilities_sum
    p_h5_given_Q = probability_5 / probabilities_sum

    file_handle.write("\nP(h1 | Q) = " + str(round(p_h1_given_Q, 5)))
    file_handle.write("\nP(h2 | Q) = " + str(round(p_h2_given_Q, 5)))
    file_handle.write("\nP(h3 | Q) = " + str(round(p_h3_given_Q, 5)))
    file_handle.write("\nP(h4 | Q) = " + str(round(p_h4_given_Q, 5)))
    file_handle.write("\nP(h5 | Q) = " + str(round(p_h5_given_Q, 5)))
    file_handle.write("\n")

    h1_prior = p_h1_given_Q
    h2_prior = p_h2_given_Q
    h3_prior = p_h3_given_Q
    h4_prior = p_h4_given_Q
    h5_prior = p_h5_given_Q

    next_is_C = h1_prior * h1_cherry + h2_prior * h2_cherry + \
                h3_prior * h3_cherry + h4_prior * h4_cherry + \
                h5_prior * h5_cherry
    file_handle.write("\nProbability that the next candy we pick will be C, given Q: " + str(round(next_is_C, 5)))

    next_is_L = h1_prior * h1_lime + h2_prior * h2_lime + \
                h3_prior * h3_lime + h4_prior * h4_lime + \
                h5_prior * h5_lime
    file_handle.write("\nProbability that the next candy we pick will be L, given Q: " + str(round(next_is_L, 5)))
    file_handle.write("\n")

    if after_observation == len(observations):
        break
    else:
        after_observation = after_observation + 1
        observations_index = observations_index + 1
