import sys
import calculate_probability

try:
    all_event_arguments_list = sys.argv[1:]
except IndexError:
    print("You have to input at least one argument ")
    sys.exit(1)

# Let's say that we have argument Bf At Mt
# We calculate P(Bf ∧ At ∧ Mt)
if "given" not in all_event_arguments_list:
    probability1 = calculate_probability.calculate_probability(all_event_arguments_list)
    print("Probability = " + str(probability1))
# Let's say that we have argument Jt given Et Bf
# We calculate P1(Jt ∧ Et ∧ Bf)
# Then we calculate P2(Et ∧ Bf)
# Then we calculate p1 / p2
else:
    # Extract event arguments after given
    after_given_event_arguments = []
    for index in range(0, len(all_event_arguments_list)):
        if all_event_arguments_list[index] == "given":
            after_given_event_arguments.extend(all_event_arguments_list[index + 1:])
            break
    probability1 = calculate_probability.calculate_probability(all_event_arguments_list)
    probability2 = calculate_probability.calculate_probability(after_given_event_arguments)
    print("Probability = " + str(probability1 / probability2))
