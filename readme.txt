Name: Yanzhi Wang
UTA ID: 1001827416
The programming language: Python 3

How the code is structured:
1. compute_a_posteriori.py
1.1 The program store the argument in observations
1.2 Hardcode the h1 to h5 prior, cherry probability and lime probability
1.3 Create result.txt then we can write to it
1.4 If current observations is cherry, calculate each prior * cherry probability, then sum up. 
If current observations lime, calculate each prior * lime probability, then sum up. 
Then calculate p_h_given_Q. The p_h_given_Q is prior for the next observations. 
Then calculate probability that the next candy we pick will be C, given Q. 
Then calculate probability that the next candy we pick will be L, given Q. 
Then repeat the same process for all the observations. 

2. bnet.py
2.1 The program store the event and boolean argument in list
2.2 If there is no "given" in argument,
let's say that we have argument Bf At Mt,
we calculate probability P(Bf ∧ At ∧ Mt). 
If there is "given" in argument,
let's say that we have argument Jt given Et Bf,
we calculate P1(Jt ∧ Et ∧ Bf),
then we calculate P2(Et ∧ Bf),
then we calculate p1 / p2. 
2.3 How do we calculate the probability
2.3.1 import calculate_probability
2.3.2 In the calculate_probability.py,
we store the event in dictionary. 
The key is event and the value is boolean. 
Let's say that the argument is Jt given Et Bf, 
then we have the dictionary {'J': True, 'E': True, 'B': False}. 
2.3.3 If you specify "B", "E", "A", "J", "M" to be true, then it always true.
If you specify "B", "E", "A", "J", "M" to be false, then it always false.
Otherwise, it can be either true or false.
Then we can make a truth table for "B", "E", "A", "J", "M". 
I am using itertools to make a truth table. 
The truth table is the list contain tuple
[(False, False, False, False, False),
 (False, False, False, False, True),
 (False, False, False, True, False)]
2.3.4 Hardcode the truth values from the table. Then calculate the joint distribution. 
2.3.5 Then we create the joint_distribution_table dictionary. 
The key is the truth value tuple and the value is the corresponding probability. 
{(False, False, False, False, False): 0.9367427006190001,
 (False, False, False, False, True): 0.009462047481000001,
 (False, False, False, True, False): 0.04930224740100002} 
2.3.6 Then we calculate the probability. 
We traverse the joint_distribution_table, then we look for certain truth value pattern. 
Let's say that the pattern we are looking for is (False, False, False, False, True). 
We put it into joint_distribution_table dictionary as key and get the value. 
The value is the corresponding probability. 






   