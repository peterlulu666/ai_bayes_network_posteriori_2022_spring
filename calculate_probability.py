import itertools


def calculate_probability(event_arguments_list):
    # Store the event in dictionary
    # The key is event and the value is boolean
    # Let's say that the argument is Jt given Et Bf
    # We have the dictionary {'J': True, 'E': True, 'B': False}
    T = bool(True)
    F = bool(False)
    arguments_dict = {}
    for argument in event_arguments_list:
        if argument != "given":
            arguments_dict[argument[0]] = (T if argument[1] == "t" else F)

    # https://stackoverflow.com/questions/29663415/generate-truth-tables-with-some-variables-already-set-in-python
    # If you specify event to be true, then it always true
    # If you specify event to be false, then it always false
    # Otherwise, it can be either true or false
    possible = []
    for event in ["B", "E", "A", "J", "M"]:
        possible.append([arguments_dict[event]]) if event in arguments_dict else possible.append([T, F])
    # Make a truth table listing all combinations of values of your
    # variables (if there are M Boolean variables then the table will have 2^M rows)
    truth_table_list = []
    for truth_value_tuple in itertools.product(*possible):
        truth_table_list.append(truth_value_tuple)

    # Calculate the joint distribution
    def calculate_joint_distribution(truth_value):
        b, e, a, j, m = truth_value
        # hardcode the truth values from the table
        # P(B)
        if b == True:
            burglary_node = 0.001
        # P(¬B)
        if b == False:
            burglary_node = 1 - 0.001
        # P(E)
        if e == True:
            earthquake_node = 0.002
        # P(¬E)
        if e == False:
            earthquake_node = 1 - 0.002
        # P(A | B, E)
        if a == True and b == True and e == True:
            alarm_node = 0.95
        # P(A | B, ¬E)
        if a == True and b == True and e == False:
            alarm_node = 0.94
        # P(A | ¬B, E)
        if a == True and b == False and e == True:
            alarm_node = 0.29
        # P(A | ¬B, ¬E)
        if a == True and b == False and e == False:
            alarm_node = 0.001
        # P(¬A | B, E)
        if a == False and b == True and e == True:
            alarm_node = 1 - 0.95
        # P(¬A | B, ¬E)
        if a == False and b == True and e == False:
            alarm_node = 1 - 0.94
        # P(¬A | ¬B, E)
        if a == False and b == False and e == True:
            alarm_node = 1 - 0.29
        # P(¬A | ¬B, ¬E)
        if a == False and b == False and e == False:
            alarm_node = 1 - 0.001
        # P(J | A)
        if j == True and a == True:
            JohnCalls_node = 0.9
        # P(J | ¬A)
        if j == True and a == False:
            JohnCalls_node = 0.05
        # P(¬J | A)
        if j == False and a == True:
            JohnCalls_node = 1 - 0.9
        # P(¬J | ¬A)
        if j == False and a == False:
            JohnCalls_node = 1 - 0.05
        # P(M | A)
        if m == True and a == True:
            MaryCalls_node = 0.7
        # P(M | ¬A)
        if m == True and a == False:
            MaryCalls_node = 0.01
        # P(¬M | A)
        if m == False and a == True:
            MaryCalls_node = 1 - 0.7
        # P(¬M | ¬A)
        if m == False and a == False:
            MaryCalls_node = 1 - 0.01

        return burglary_node * earthquake_node * alarm_node * JohnCalls_node * MaryCalls_node

    # Create the joint_distribution_table dictionary
    # The key is truth value tuple and the value is the corresponding probability
    joint_distribution_table = {}
    for tup in truth_table_list:
        joint_distribution_table[tup] = calculate_joint_distribution(tup)

    # Calculate probability
    probability = 0
    for tup in truth_table_list:
        probability = probability + joint_distribution_table[tup]

    return probability
